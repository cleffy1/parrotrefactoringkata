package parrot;

abstract class Parrot {

    Parrot() {
    }

    abstract public double getSpeed();

    double getBaseSpeed(double voltage) {
        return Math.min(24.0, voltage * getBaseSpeed());
    }

    double getLoadFactor() {
        return 9.0;
    }

    double getBaseSpeed() {
        return 12.0;
    }

}
